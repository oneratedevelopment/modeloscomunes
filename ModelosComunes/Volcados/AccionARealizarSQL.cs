﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelosComunes.Volcados
{
    public class AccionARealizarSQL
    {
        /// <summary>
        /// Cadena de conexión a la BBDD
        /// </summary>
        public string CadenaConexion { get; set; }

        /// <summary>
        /// Query a ejecutar de base
        /// </summary>
        public string QueryBase { get; set; }

        /// <summary>
        /// TimeOut de la llamada
        /// </summary>
        public int TimeOut { get; set; }

        /// <summary>
        /// La acción se considera crítica y debe realizarse
        /// </summary>
        public bool Critico { get; set; }

    }
}
