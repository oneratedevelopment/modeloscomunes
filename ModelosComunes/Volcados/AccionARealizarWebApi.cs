﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelosComunes.Volcados
{
    public class AccionARealizarWebApi
    {
        /// <summary>
        /// URL a llamar
        /// </summary>
        public string URL { get; set; }

        /// <summary>
        /// Credenciales del servicio
        /// </summary>
        public string Usuario { get; set; }

        /// <summary>
        /// Credenciales del servicio
        /// </summary>
        public string Password { get; set; }

        /// <summary>
        /// Identifica si se trate de un GET o un POST
        /// </summary>
        public string Metodo { get; set; }

        /// <summary>
        /// TimeOut de la llamada
        /// </summary>
        public int TimeOut { get; set; }

        /// <summary>
        /// La acción se considera crítica y debe realizarse
        /// </summary>
        public bool Critico { get; set; }

    }
}
