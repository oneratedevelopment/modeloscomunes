﻿using ModelosComunes.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelosComunes.Volcados
{
    public class FicheroComprimidoVolcado
    {

        /// <summary>
        /// Codigo del fichero (Clientes, facturas, ...)
        /// </summary>
        public string FicheroCodigo { get; set; }

        /// <summary>
        /// Nombres de los ficheros
        /// </summary>
        public List<string> FicheroNombres { get; set; }

        /// <summary>
        /// Expresion regular o norma que debe cumplir el nombre de los ficheros esperados
        /// </summary>
        public string FicheroExpresionNombre { get; set; }

        /// <summary>
        /// En caso de no ser nulo ni espacio en blanco, el texto formatDate() de la expresion regular se sustituirá por DateTime.Now.ToString(FicheroExpresionFechaFormato)
        /// </summary>
        public string FicheroExpresionFechaFormato { get; set; }

        /// <summary>
        /// Ordenacion de ficheros que cumplen con la expresion
        /// </summary>
        public TiposOrdenacion TipoOrdenacion { get; set; }

        /// <summary>
        /// Determina si se trata de un fichero secuencial
        /// </summary>
        public TiposSecuencia TipoSecuencia { get; set; }

        /// <summary>
        /// Expresion regular o norma que debe cumplir el elemento que determina la ordenacion de la secuencia
        /// </summary>
        public string FicheroExpresionSecuencia { get; set; }

        /// <summary>
        /// Determina los ficheros se han descomprimido correctamente     
        /// **IMPORTANTE - (Si el estado está marcado como NO_APLICA, la secuencia no debe tratar de comprobarse) **
        /// </summary>
        public EstadosValidacion FicheroDescompresionOK { get; set; }

        /// <summary>
        /// Observaciones de validación de secuencia del fichero (Se puede usar para recoger errores)
        /// </summary>
        public string FicheroSecuenciaObservaciones { get; set; }

        /// <summary>
        /// Tipo de fichero (4:ZIP, 5:RAR)
        /// </summary>
        public TiposFichero FicheroTipoFichero { get; set; }

        /// <summary>
        /// Determina si el fichero se ha encontrado o no o si se ha descartado por no cumplir con normas de validación
        /// **IMPORTANTE - (Si el estado está marcado como NO_APLICA, el fichero no debe tratar de comprobarse) **
        /// </summary>
        public EstadosFichero FicheroEstado { get; set; }

        /// <summary>
        /// Otras observaciones del estado del fichero
        /// </summary>
        public string FicheroEstadoObservaciones { get; set; }

        /// <summary>
        /// Ruta en la que se espera enontrar el fichero
        /// </summary>
        public string FicheroRuta { get; set; }

        /// <summary>
        /// Ruta en la que se descomprime el fichero
        /// </summary>
        public string FicheroRutaDescompresion { get; set; }

        /*/// <summary>
        /// Ruta a la que se mueve el fichero si se descarta
        /// </summary>
        public string FicheroRutaCuarentena { get; set; }*/

        /// <summary>
        /// Ruta en la que se copia el fichero antes de descomprimirlo (Si no se especifica, el fichero no se copia)
        /// </summary>
        public string FicheroRutaCopia { get; set; }

        /// <summary>
        /// Expresión que se seguirá para renombrar el fichero tras la descompresión
        /// </summary>
        public string FicheroRenombrarTrasDescompresion { get; set; }

        /// <summary>
        /// Expresión regular que se seguirá para renombrar los ficheros descomprimidos
        /// </summary>
        public string FicheroRenombrarDescomprimidos { get; set; }
        
        /// <summary>
        /// Determina si el fichero es de aparicion opcional, si se marca a True su ausencia no debe producir un KO
        /// </summary>
        public bool Opcional { get; set; }


        public FicheroComprimidoVolcado()
        {
            FicheroNombres = new List<string>();
        }
        public bool ComprobarFicheroOk()
        {
            return (
                (FicheroDescompresionOK == EstadosValidacion.NO_APLICA || FicheroDescompresionOK == EstadosValidacion.OK)
                && (FicheroEstado == EstadosFichero.ENCONTRADO || Opcional)
                );
        }
    }
}
