﻿using ModelosComunes.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelosComunes.Volcados
{
    public class FicheroVolcado
    {
        /// <summary>
        /// Codigo del fichero (Clientes, facturas, ...)
        /// </summary>
        public string FicheroCodigo { get; set; }

        /// <summary>
        /// Nombre del fichero
        /// </summary>
        public List<string> FicheroNombres { get; set; }

        /// <summary>
        /// Expresion regular o norma que debe cumplir el nombre de fichero esperado
        /// </summary>
        public string FicheroExpresionNombre { get; set; }

        /// <summary>
        /// En caso de no ser nulo ni espacio en blanco, el texto formatDate() de la expresion regular se sustituirá por DateTime.Now.ToString(FicheroExpresionFechaFormato)
        /// </summary>
        public string FicheroExpresionFechaFormato { get; set; }


        /// <summary>
        /// Ordenacion de ficheros que cumplen con la expresion
        /// </summary>
        public TiposOrdenacion TipoOrdenacion { get; set; }

        /// <summary>
        /// Determina si se trata de un fichero secuencial
        /// </summary>
        public TiposSecuencia TipoSecuencia { get; set; }

        /// <summary>
        /// Expresion regular o norma que debe cumplir el elemento que determina la ordenacion de la secuencia
        /// </summary>
        public string FicheroExpresionSecuencia { get; set; }

        /// <summary>
        /// Determina los ficheros cumplen con la secuencia        
        /// **IMPORTANTE - (Si el estado está marcado como NO_APLICA, la secuencia no debe tratar de comprobarse) **
        /// </summary>
        public EstadosValidacion FicheroSecuenciaOK { get; set; }

        /// <summary>
        /// Observaciones de validación de secuencia del fichero (Se puede usar para recoger errores)
        /// </summary>
        public string FicheroSecuenciaObservaciones { get; set; }

        /// <summary>
        /// Tipo de fichero (CSV, MDB, ETC...)
        /// </summary>
        public TiposFichero FicheroTipoFichero { get; set; }

        /// <summary>
        /// Para el caso de CSV, delimitador de las columnas
        /// </summary>
        public string FicheroDelimitadorColumna { get; set; }

        /// <summary>
        /// Para el caso de CSV, delimitador de las filas
        /// </summary>
        public string FicheroDelimitadorFila { get; set; }

        /// <summary>
        /// Para el caso de CSV, Code Page
        /// </summary>
        public string FicheroCodePage { get; set; }

        /// <summary>
        /// Para el caso de CSV, Fila en la que comienzan los datos
        /// </summary>
        public int FicheroPrimeraFila { get; set; }

        /// <summary>
        /// Determina si el fichero se ha encontrado o no o si se ha descartado por no cumplir con normas de validación
        /// **IMPORTANTE - (Si el estado está marcado como NO_APLICA, el fichero no debe tratar de comprobarse) **
        /// </summary>
        public EstadosFichero FicheroEstado { get; set; }

        /// <summary>
        /// Otras observaciones del estado del fichero
        /// </summary>
        public string FicheroEstadoObservaciones { get; set; }

        /// <summary>
        /// Determina si el formato del fichero es correcto        
        /// **IMPORTANTE - (Si el estado está marcado como NO_APLICA, el formato no debe tratar de comprobarse) **
        /// </summary>
        public EstadosValidacion FicheroFormatoOK { get; set; }

        /// <summary>
        /// Observaciones del formato del fichero (Se puede usar para recoger errores)
        /// </summary>
        public string FicheroFormatoObservaciones { get; set; }

        /// <summary>
        /// Errores de formato que detendrán la comprobación de formato. (-1 => Ilimitado)
        /// </summary>
        public int FicheroFormatoErroresRestantes { get; set; }

        /// <summary>
        /// Lista de las columnas que se esperan en el fichero
        /// </summary>
        public List<ColumnaFicheroVolcado> FicheroColumnas { get; set; }

        /// <summary>
        /// Determina si se debe rellenar la propiedad FicheroFilasValores
        /// </summary>
        public bool FicheroFilasIncluirValores { get; set; }

        /// <summary>
        /// Valores de cada una de las filas
        /// </summary>
        public List<string[]> FicheroFilasValores { get; set; }

        /// <summary>
        /// Determina si las columnas del fichero cumplen con las características especificadas     
        /// **IMPORTANTE - (Si el estado está marcado como NO_APLICA, las columnas no se deben validar) **
        /// </summary>
        public EstadosValidacion FicheroColumnasOK { get; set; }

        /// <summary>
        /// Observaciones de Columnas del fichero (Se puede usar para recoger errores)
        /// </summary>
        public string FicheroColumnasObservaciones { get; set; }

        /// <summary>
        /// Errores de Columnas que detendrán la comprobación de Columnas. (-1 => Ilimitado)
        /// </summary>
        public int FicheroColumnasErroresRestantes { get; set; }

        /// <summary>
        /// Determina la integridad referencial de los datos del fichero   
        /// **IMPORTANTE - (Si el estado está marcado como NO_APLICA, las referencias no se deben validar) **
        /// </summary>
        public EstadosValidacion FicheroReferenciasOK { get; set; }

        /// <summary>
        /// Observaciones de integridad referencial del fichero (Se puede usar para recoger errores)
        /// </summary>
        public string FicheroReferenciasObservaciones { get; set; }

        /// <summary>
        /// Errores de integridad referencial que detendrán la comprobación de Referencias. (-1 => Ilimitado)
        /// </summary>
        public int FicheroReferenciasErroresRestantes { get; set; }
        
        /// <summary>
        /// Nombres de las columnas cuya agrupación debe ser única. (Por ejemplo PKs o PKs múltiples)
        /// </summary>
        public List<List<string>> ColumnasUnicas { get; set; }

        /// <summary>
        /// Determina que no existen problemas de columnas o conjuntos de columnas duplicadas
        /// **IMPORTANTE - (Si el estado está marcado como NO_APLICA, la comprobación de duplicados no se debe realizar) **
        /// </summary>
        public EstadosValidacion FicheroColumnasUnicasOK { get; set; }

        /// <summary>
        /// Observaciones de Columnas Unicas del fichero (Se puede usar para recoger errores)
        /// </summary>
        public string FicheroColumnasUnicasObservaciones { get; set; }

        /// <summary>
        /// Errores de Columnas Unicas que detendrán la comprobación de Columnas Unicas. (-1 => Ilimitado)
        /// </summary>
        public int FicheroColumnasUnicasErroresRestantes { get; set; }

        /// <summary>
        /// Ruta en la que se espera enontrar el fichero
        /// </summary>
        public string FicheroRuta { get; set; }

        /// <summary>
        /// Ruta a la que se mueve el fichero si se descarta (Si no se especifica, es recomendable borrar el fichero)
        /// </summary>
        public string FicheroRutaCuarentena { get; set; }

        /// <summary>
        /// Ruta en la que se copia el fichero (Si no se especifica, el fichero no se copia)
        /// </summary>
        public string FicheroRutaCopia { get; set; }

        /// <summary>
        /// Expresión regular que se seguirá para renombrar el fichero en caso de OK (Si no se especifica, el fichero no se renombra)
        /// </summary>
        public string FicheroRenombrarExpresion_CasoOK { get; set; }
        /// <summary>
        /// Expresión regular que se seguirá para renombrar el fichero en caso de KO (Si no se especifica, el fichero no se renombra)
        /// </summary>
        public string FicheroRenombrarExpresion_CasoKO { get; set; }

        /*/// <summary>
        /// Determina si las tareas de validación para un fichero deben continuar cuando se produce un KO
        /// </summary>
        public bool ComprobacionExhaustiva { get; set; }*/

        /// <summary>
        /// Determina si el fichero es de aparicion opcional, si se marca a True su ausencia no debe producir un KO
        /// </summary>
        public bool Opcional { get; set; }

        /// <summary>
        /// Numero de hilos asignados al procesamiento (Minimo 1)
        /// </summary>
        public int FicheroProcesoHilosAsignados { get; set; }

        /// <summary>
        /// Numero de Lineas leidas por bloque (Mínimo 1)
        /// </summary>
        public int FicheroProcesoLineasPorBloque { get; set; }


        public FicheroVolcado()
        {
            FicheroNombres = new List<string>();
            FicheroColumnas = new List<ColumnaFicheroVolcado>();
            FicheroFilasValores = new List<string[]>();
            ColumnasUnicas = new List<List<string>>();
        }

        public bool ComprobarAlgunaColumnaActiva()
        {
            return FicheroColumnas.Any(x =>
            x.ColumnaLongitudMaximaOK != EstadosValidacion.NO_APLICA
            || x.ColumnaNullableOK != EstadosValidacion.NO_APLICA
            || x.ColumnaTipoDatoOK != EstadosValidacion.NO_APLICA);
        }

        public List<ColumnaFicheroVolcado> ObtenerColumnasActivas()
        {
            return FicheroColumnas.Where(x =>
            x.ColumnaLongitudMaximaOK != EstadosValidacion.NO_APLICA
            || x.ColumnaNullableOK != EstadosValidacion.NO_APLICA
            || x.ColumnaTipoDatoOK != EstadosValidacion.NO_APLICA).ToList();
        }

        public bool ComprobarFicheroOk()
        {
            return (
                (FicheroColumnasOK == EstadosValidacion.NO_APLICA || FicheroColumnasOK == EstadosValidacion.OK 
                || (Opcional && (FicheroColumnasOK == EstadosValidacion.DESCONOCIDO || FicheroColumnasOK == EstadosValidacion.OMITIDO)))
                && (FicheroSecuenciaOK == EstadosValidacion.NO_APLICA || FicheroSecuenciaOK == EstadosValidacion.OK
                || (Opcional && (FicheroSecuenciaOK == EstadosValidacion.DESCONOCIDO || FicheroSecuenciaOK == EstadosValidacion.OMITIDO)))
                && (FicheroColumnasUnicasOK == EstadosValidacion.NO_APLICA || FicheroColumnasUnicasOK == EstadosValidacion.OK
                || (Opcional && (FicheroColumnasUnicasOK == EstadosValidacion.DESCONOCIDO || FicheroColumnasUnicasOK == EstadosValidacion.OMITIDO)))
                && (FicheroFormatoOK == EstadosValidacion.NO_APLICA || FicheroFormatoOK == EstadosValidacion.OK
                || (Opcional && (FicheroFormatoOK == EstadosValidacion.DESCONOCIDO || FicheroFormatoOK == EstadosValidacion.OMITIDO)))
                && (FicheroReferenciasOK == EstadosValidacion.NO_APLICA || FicheroReferenciasOK == EstadosValidacion.OK
                || (Opcional && (FicheroReferenciasOK == EstadosValidacion.DESCONOCIDO || FicheroReferenciasOK == EstadosValidacion.OMITIDO)))
                && (FicheroEstado == EstadosFichero.ENCONTRADO || Opcional));
        }
        public void AutoValidarColumnas()
        {
            FicheroColumnas.ForEach(s => s.AutoValidar());
            if (FicheroColumnasOK != EstadosValidacion.NO_APLICA && ComprobarAlgunaColumnaActiva() && FicheroEstado == EstadosFichero.ENCONTRADO)
            {
                FicheroColumnasOK = ObtenerColumnasActivas().Any(x => !x.ComprobarColumnaOk()) ?
                EstadosValidacion.KO : EstadosValidacion.OK;
            }
        }
    }
}
