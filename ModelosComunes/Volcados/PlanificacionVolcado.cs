﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelosComunes.Volcados
{
    public class PlanificacionVolcado
    {
        public bool L { get; set; }
        public bool M { get; set; }
        public bool X { get; set; }
        public bool J { get; set; }
        public bool V { get; set; }
        public bool S { get; set; }
        public bool D { get; set; }
        public int Hora { get; set; }
        public int Minutos { get; set; }

        public DateTime? ObtenerPlanificacionAnterior(DateTime toCompare)
        {
            List<DateTime> dateTimes = new List<DateTime>();
            DateTime ComienzoSemana = toCompare.AddDays(-1 * (7 + (toCompare.DayOfWeek - DayOfWeek.Monday)) % 7).Date;
            List<DateTime> dateTimesFixed = new List<DateTime>();

            if (L)
                dateTimes.Add(ComienzoSemana.AddHours(Hora).AddMinutes(Minutos));
            if (M)
                dateTimes.Add(ComienzoSemana.AddDays(1).AddHours(Hora).AddMinutes(Minutos));
            if (X)
                dateTimes.Add(ComienzoSemana.AddDays(2).AddHours(Hora).AddMinutes(Minutos));
            if (J)
                dateTimes.Add(ComienzoSemana.AddDays(3).AddHours(Hora).AddMinutes(Minutos));
            if (V)
                dateTimes.Add(ComienzoSemana.AddDays(4).AddHours(Hora).AddMinutes(Minutos));
            if (S)
                dateTimes.Add(ComienzoSemana.AddDays(5).AddHours(Hora).AddMinutes(Minutos));
            if (D)
                dateTimes.Add(ComienzoSemana.AddDays(6).AddHours(Hora).AddMinutes(Minutos));

            

            foreach (DateTime date in dateTimes)
            {
                if (date > toCompare)
                    dateTimesFixed.Add(date.AddDays(-7));
                else
                    dateTimesFixed.Add(date);
            }

            dateTimesFixed = dateTimesFixed.Where(x => (toCompare.Ticks - x.Ticks) > 0).ToList();

            /*if (!dateTimesFixed.Any())
                return null;*/

            return dateTimesFixed.OrderBy(x => (toCompare.Ticks - x.Ticks)).FirstOrDefault();

        }
    }
}
