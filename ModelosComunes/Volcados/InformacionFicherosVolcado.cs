﻿using ModelosComunes.Enums;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;

namespace ModelosComunes.Volcados
{
    public class InformacionFicherosVolcado
    {
        /// <summary>
        /// Identificador de la importación
        /// </summary>
        public string Codigo { get; set; }

        /// <summary>
        /// Nombre de la empresa
        /// </summary>
        public string EmpresaNombre { get; set; }

        /// <summary>
        /// Campo auxiliar
        /// </summary>
        public string Auxiliar { get; set; }

        /// <summary>
        /// Lista de ficheros a descomprimir esperados y su configuración
        /// </summary>
        public List<FicheroCopiaPreProceso> FicherosCopiaPreProceso { get; set; }

        /// <summary>
        /// Lista de ficheros a descomprimir esperados y su configuración
        /// </summary>
        public List<FicheroComprimidoVolcado> FicherosComprimidos { get; set; }

        /// <summary>
        /// Lista de ficheros esperados y su configuración
        /// </summary>
        public List<FicheroVolcado> Ficheros { get; set; }

        /// <summary>
        /// Lista de acciones a realizar si todo está correcto 
        /// </summary>
        public List<AccionARealizarWebApi> AccionesARealizarWebApi_CasoOK { get; set; }

        /// <summary>
        /// Lista de acciones que se realizarán si falta o falla algo 
        /// </summary>
        public List<AccionARealizarWebApi> AccionesARealizarWebApi_CasoKO { get; set; }

        /// <summary>
        /// Lista de acciones que se realizarán siempre
        /// </summary>
        public List<AccionARealizarWebApi> AccionesARealizarWebApi_CasoSIEMPRE { get; set; }

        /// <summary>
        /// Lista de emails a enviar si todo está correcto 
        /// </summary>
        public List<AccionARealizarEmail> AccionesARealizarEmail_CasoOK { get; set; }

        /// <summary>
        /// Lista de emails a enviar si falta o falla algo 
        /// </summary>
        public List<AccionARealizarEmail> AccionesARealizarEmail_CasoKO { get; set; }

        /// <summary>
        /// Lista de emails a enviar siempre
        /// </summary>
        public List<AccionARealizarEmail> AccionesARealizarEmail_CasoSIEMPRE { get; set; }
        
        /// <summary>
        /// Lista de SQL a ejecutar si todo está correcto 
        /// </summary>
        public List<AccionARealizarSQL> AccionesARealizarSQL_CasoOK { get; set; }

        /// <summary>
        /// Lista de SQL a ejecutar si falta o falla algo 
        /// </summary>
        public List<AccionARealizarSQL> AccionesARealizarSQL_CasoKO { get; set; }

        /// <summary>
        /// Lista de SQL a ejecutar siempre
        /// </summary>
        public List<AccionARealizarSQL> AccionesARealizarSQL_CasoSIEMPRE { get; set; }

        /// <summary>
        /// Lista de volcados planificados
        /// </summary>
        public List<PlanificacionVolcado> VolcadosPlanificados { get; set; }

        public InformacionFicherosVolcado()
        {
            FicherosCopiaPreProceso = new List<FicheroCopiaPreProceso>();
            FicherosComprimidos = new List<FicheroComprimidoVolcado>();
            Ficheros = new List<FicheroVolcado>();
            AccionesARealizarWebApi_CasoOK = new List<AccionARealizarWebApi>();
            AccionesARealizarWebApi_CasoKO = new List<AccionARealizarWebApi>();
            AccionesARealizarWebApi_CasoSIEMPRE = new List<AccionARealizarWebApi>();
            AccionesARealizarEmail_CasoOK = new List<AccionARealizarEmail>();
            AccionesARealizarEmail_CasoKO = new List<AccionARealizarEmail>();
            AccionesARealizarEmail_CasoSIEMPRE = new List<AccionARealizarEmail>();
            AccionesARealizarSQL_CasoOK = new List<AccionARealizarSQL>();
            AccionesARealizarSQL_CasoKO = new List<AccionARealizarSQL>();
            AccionesARealizarSQL_CasoSIEMPRE = new List<AccionARealizarSQL>();
            VolcadosPlanificados = new List<PlanificacionVolcado>();
        }
        public bool ComprobarAlgunFicheroActivo()
        {
            return Ficheros.Any(x => x.FicheroEstado != EstadosFichero.NO_APLICA && (
            x.FicheroColumnasOK != EstadosValidacion.NO_APLICA
            || x.FicheroColumnasUnicasOK != EstadosValidacion.NO_APLICA
            || x.FicheroFormatoOK != EstadosValidacion.NO_APLICA
            || x.FicheroReferenciasOK != EstadosValidacion.NO_APLICA));
        }

        public List<FicheroVolcado> ObtenerFicherosActivos()
        {
            return Ficheros.Where(x => x.FicheroEstado != EstadosFichero.NO_APLICA && (
            x.FicheroColumnasOK != EstadosValidacion.NO_APLICA
            || x.FicheroColumnasUnicasOK != EstadosValidacion.NO_APLICA
            || x.FicheroFormatoOK != EstadosValidacion.NO_APLICA
            || x.FicheroReferenciasOK != EstadosValidacion.NO_APLICA)).ToList();
        }

        public bool ComprobarAlgunFicheroComprimidoActivo()
        {
            return FicherosComprimidos.Any(x => x.FicheroEstado != EstadosFichero.NO_APLICA 
            /*&& x.FicheroDescompresionOK != EstadosValidacion.NO_APLICA*/);
        }

        public List<FicheroComprimidoVolcado> ObtenerFicherosComprimidosActivos()
        {
            return FicherosComprimidos.Where(x => x.FicheroEstado != EstadosFichero.NO_APLICA
            /*&& x.FicheroDescompresionOK != EstadosValidacion.NO_APLICA*/).ToList();
        }

        public bool ComprobarAlgunFicheroCopiaPreProcesoActivo()
        {
            return FicherosCopiaPreProceso.Any(x => x.FicheroEstado != EstadosFichero.NO_APLICA
            && x.FicheroCopiaOK != EstadosValidacion.NO_APLICA);
        }

        public List<FicheroCopiaPreProceso> ObtenerFicherosCopiaPreProcesoActivos()
        {
            return FicherosCopiaPreProceso.Where(x => x.FicheroEstado != EstadosFichero.NO_APLICA
            && x.FicheroCopiaOK != EstadosValidacion.NO_APLICA).ToList();
        }

        /// <summary>
        /// Obtiene un clon del InformacionFicherosVolcado original
        /// </summary>
        /// <param name="original">Objeto a clonar</param>
        /// <returns>Objeto clonado</returns>
        public static InformacionFicherosVolcado ObtenerClon(InformacionFicherosVolcado original)
        {
            InformacionFicherosVolcado clon = new InformacionFicherosVolcado();
            if (original == null)
            {
                return clon;
            }

            IFormatter formatter = new BinaryFormatter();
            Stream stream = new MemoryStream();
            using (stream)
            {
                formatter.Serialize(stream, original);
                stream.Seek(0, SeekOrigin.Begin);
                return (InformacionFicherosVolcado)formatter.Deserialize(stream);
            }

        }
        public DateTime? ObtenerPlanificacionAnterior(DateTime toCompare)
        {
            List<DateTime> dateTimes = new List<DateTime>();
            DateTime ComienzoSemana = toCompare.AddDays(-1 * (7 + (toCompare.DayOfWeek - DayOfWeek.Monday)) % 7).Date;
            List<DateTime> dateTimesFixed = new List<DateTime>();

            foreach (PlanificacionVolcado volcado in VolcadosPlanificados.Where(x => x.ObtenerPlanificacionAnterior(toCompare) != null))
                dateTimes.Add(volcado.ObtenerPlanificacionAnterior(toCompare).Value);
            foreach (DateTime date in dateTimes)
            {
                if (date > toCompare)
                    dateTimesFixed.Add(date.AddDays(-7));
                else
                    dateTimesFixed.Add(date);
            }

            dateTimesFixed = dateTimesFixed.Where(x => (toCompare.Ticks - x.Ticks) > 0).ToList();

            /*if (!dateTimesFixed.Any())
                return null;*/

            return dateTimesFixed.OrderBy(x => (toCompare.Ticks - x.Ticks)).FirstOrDefault();

        }

    }
}
