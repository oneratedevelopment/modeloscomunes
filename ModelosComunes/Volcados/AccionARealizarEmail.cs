﻿using ModelosComunes.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelosComunes.Volcados
{
    public class AccionARealizarEmail
    {
        /// <summary>
        /// Destinatario del mensaje
        /// </summary>
        public List<string> Destinatarios { get; set; }

        /// <summary>
        /// Asunto del email
        /// </summary>
        public string Asunto { get; set; }

        /// <summary>
        /// Texto previo al mensaje auto generado en función del TipoMensajeEmail
        /// </summary>
        public string Cuerpo { get; set; }

        /// <summary>
        /// Tipo de mensaje a generar automaticamente
        /// </summary>
        public TiposMensajeEmail TipoMensaje { get; set; }

        /// <summary>
        /// La acción se considera crítica y debe realizarse
        /// </summary>
        public bool Critico { get; set; }

        public AccionARealizarEmail()
        {
            Destinatarios = new List<string>();
        }

    }
}
