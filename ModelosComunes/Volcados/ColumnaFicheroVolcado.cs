﻿using ModelosComunes.Enums;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace ModelosComunes.Volcados
{
    /// <summary>
    /// Datos de cada columna del fichero de volcado:
    /// *** Si ColumnaTipoDato es NO_COMPROBAR, no se debe realizar la comprobación de tipo de dato ***
    /// *** Si ColumnaLongitudMaxima es 0 se considera longitud máxima ***
    /// *** Si columnaOK es NO_APLICA, no se debe realizar ninguna comprobación para esta columna (similar a OK) ***
    /// </summary>
    public class ColumnaFicheroVolcado
    {
        /// <summary>
        /// Nombre de la columna
        /// </summary>
        public string ColumnaNombre { get; set; }
        
        /*/// <summary>
        /// Se puede utilizar para incluir los valores de la columna
        /// </summary>
        public object[] ColumnaValores { get; set; }*/

        /// <summary>
        /// Posición en la que se encuentra la columna
        /// </summary>
        public int ColumnaPosicion { get; set; }

        /// <summary>
        /// Tipo de dato de la columna
        /// </summary>
        public TiposDatoColumna ColumnaTipoDato { get; set; }

        /// <summary>
        /// Expresion regular que debe cumplir una columna de tipo de dato EXPRESION 
        /// O el Formato de fecha para las DATE
        /// </summary>
        public string ColumnaTipoDatoExpresion { get; set; }

        private Regex _compiledRegex = null;
        public Regex ColumnaTipoDatoExpresionRegex {
            get
            {
                if (_compiledRegex == null)
                    _compiledRegex = new Regex(ColumnaTipoDatoExpresion);

                return _compiledRegex;
            }
        }

        /// <summary>
        /// Para especifica el codigo para generar el culture info
        /// </summary>
        public string ColumnaTipoDatoCulture { get; set; }

        private CultureInfo _compiledCulture = null;
        public CultureInfo ColumnaTipoDatoExpresionCultureInfo
        {
            get
            {
                if (_compiledCulture == null)
                    _compiledCulture = CultureInfo.CreateSpecificCulture(ColumnaTipoDatoCulture);

                return _compiledCulture;
            }
        }

        /// <summary>
        /// Para los decimales, especifica el NumberStyles correspondiente
        /// </summary>
        public NumberStyles ColumnaTipoDatoNumberStyles { get; set; }

        /// <summary>
        /// Determina si la columna cumple con las especificaciones de tipo de dato
        /// **IMPORTANTE - (Si el estado está marcado como NO_APLICA, la validación no se debe realizar) **
        /// </summary>
        public EstadosValidacion ColumnaTipoDatoOK { get; set; }

        /// <summary>
        /// Longitud máxima de la columna
        /// **IMPORTANTE - (Si es 0 se considera máxima) **
        /// </summary>
        public int ColumnaLongitudMaxima { get; set; }

        /// <summary>
        /// Determina si la columna cumple con las especificaciones de longitud
        /// **IMPORTANTE - (Si el estado está marcado como NO_APLICA, la validación no se debe realizar) **
        /// </summary>
        public EstadosValidacion ColumnaLongitudMaximaOK { get; set; }

        /// <summary>
        /// Determina si la columna es nullable
        /// </summary>
        public TiposNullable ColumnaNullable { get; set; }

        /// <summary>
        /// Determina si la columna cumple con las especificaciones de nulos y no nulos
        /// **IMPORTANTE - (Si el estado está marcado como NO_APLICA, la validación no se debe realizar) **
        /// </summary>
        public EstadosValidacion ColumnaNullableOK { get; set; }
        
        /*
        /// <summary>
        /// Determina si la columna cumple con las especificaciones
        /// **IMPORTANTE - (Si el estado está marcado como NO_APLICA, la validación no se debe realizar) **
        /// </summary>
        public EstadosValidacion ColumnaOK { get; set; }*/


        /*/// <summary>
        /// Determina si las tareas de validación para una columna deben continuar cuando se produce un KO
        /// </summary>
        public bool ComprobacionExhaustiva { get; set; }*/



        public bool ComprobarColumnaOk()
        {
            return (
                (ColumnaLongitudMaximaOK == EstadosValidacion.NO_APLICA || ColumnaLongitudMaximaOK == EstadosValidacion.OK)
                && (ColumnaNullableOK == EstadosValidacion.NO_APLICA || ColumnaNullableOK == EstadosValidacion.OK)
                && (ColumnaTipoDatoOK == EstadosValidacion.NO_APLICA || ColumnaTipoDatoOK == EstadosValidacion.OK));
        }

        public void AutoValidar()
        {
            if ((ColumnaLongitudMaximaOK != EstadosValidacion.KO)
                && (ColumnaNullableOK != EstadosValidacion.KO)
                && (ColumnaTipoDatoOK != EstadosValidacion.KO))
            {
                if (ColumnaTipoDatoOK != EstadosValidacion.NO_APLICA)
                    ColumnaTipoDatoOK = EstadosValidacion.OK;
                if (ColumnaNullableOK != EstadosValidacion.NO_APLICA)
                    ColumnaNullableOK = EstadosValidacion.OK;
                if (ColumnaLongitudMaximaOK != EstadosValidacion.NO_APLICA)
                    ColumnaLongitudMaximaOK = EstadosValidacion.OK;
            }
        }
    }
}
