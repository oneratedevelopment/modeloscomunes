﻿using ModelosComunes.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelosComunes.Volcados
{
    public class FicheroCopiaPreProceso
    {

        /// <summary>
        /// Codigo del fichero (Clientes, facturas, ...)
        /// </summary>
        public string FicheroCodigo { get; set; }

        /// <summary>
        /// Nombres de los ficheros
        /// </summary>
        public List<string> FicheroNombres { get; set; }

        /// <summary>
        /// Expresion regular o norma que debe cumplir el nombre de los ficheros esperados
        /// </summary>
        public string FicheroExpresionNombre { get; set; }

        /// <summary>
        /// En caso de no ser nulo ni espacio en blanco, el texto formatDate() de la expresion regular se sustituirá por DateTime.Now.ToString(FicheroExpresionFechaFormato)
        /// </summary>
        public string FicheroExpresionFechaFormato { get; set; }

        /// <summary>
        /// Determina los ficheros se han copiado    
        /// **IMPORTANTE - (Si el estado está marcado como NO_APLICA, no se tiene en cuenta ) **
        /// </summary>
        public EstadosValidacion FicheroCopiaOK { get; set; }

        /// <summary>
        /// Determina si el fichero se ha encontrado o no o si se ha descartado por no cumplir con normas de validación
        /// **IMPORTANTE - (Si el estado está marcado como NO_APLICA, el fichero no debe tratar de comprobarse) **
        /// </summary>
        public EstadosFichero FicheroEstado { get; set; }

        /// <summary>
        /// Otras observaciones del estado del fichero
        /// </summary>
        public string FicheroEstadoObservaciones { get; set; }

        /// <summary>
        /// Ruta en la que se espera enontrar el fichero
        /// </summary>
        public string FicheroRuta { get; set; }
        
        /// <summary>
        /// Ruta en la que se copia el fichero antes de descomprimirlo (Si no se especifica, el fichero no se copia)
        /// </summary>
        public string FicheroRutaCopia { get; set; }

        /// <summary>
        /// Expresión que se seguirá para renombrar el fichero original tras la copua
        /// </summary>
        public string FicheroRenombrarOrigen { get; set; }

        /// <summary>
        /// Expresión regular que se seguirá para renombrar los ficheros copiados en el destino
        /// </summary>
        public string FicheroRenombrarDestino { get; set; }
        
        /// <summary>
        /// Determina si el fichero es de aparicion opcional, si se marca a True su ausencia no debe producir un KO
        /// </summary>
        public bool Opcional { get; set; }

        /// <summary>
        /// Determina si el fichero se encuentra en una ruta ftp
        /// </summary>
        public bool OrigenFTP { get; set; }

        /// <summary>
        /// Usuario para el acceso a la ruta
        /// </summary>
        public string UsuarioRutaOrigen { get; set; }

        /// <summary>
        /// Password para el acceso a la ruta
        /// </summary>
        public string PasswordRutaOrigen { get; set; }


        public FicheroCopiaPreProceso()
        {
            FicheroNombres = new List<string>();
        }
        public bool ComprobarFicheroOk()
        {
            return (
                (FicheroCopiaOK == EstadosValidacion.NO_APLICA || FicheroCopiaOK == EstadosValidacion.OK)
                && (FicheroEstado == EstadosFichero.ENCONTRADO || Opcional)
                );
        }
    }
}
