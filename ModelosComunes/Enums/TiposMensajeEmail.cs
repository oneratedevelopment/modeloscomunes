﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelosComunes.Enums
{
    public enum TiposMensajeEmail
    {
        NINGUNO = 0,
        OK = 1,
        OK_FECHA = 2,
        OK_FECHA_DETALLES = 3,
        OK_FECHA_DETALLES_TRAZAS = 4,
        OK_FECHA_DETALLES_TRAZAS_ADJUNTOS = 5,
        KO = 6,
        KO_FECHA = 7,
        KO_FECHA_DETALLES = 8,
        KO_FECHA_DETALLES_TRAZAS = 9,
        KO_FECHA_DETALLES_TRAZAS_ADJUNTOS = 10
    }
}
