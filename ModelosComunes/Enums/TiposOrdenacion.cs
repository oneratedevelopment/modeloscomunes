﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelosComunes.Enums
{
    public enum TiposOrdenacion
    {
        ALFABETICO_ASCENDENTE = 0,
        ALFABETICO_DESCENDENTE = 1,
        FECHA_CREACION_ASCENDENTE = 2,
        FECHA_CREACION_DESCENDENTE = 3,
        FECHA_MODIFICACION_ASCENDENTE = 4,
        FECHA_MODIFICACION_DESCENDENTE = 5,
        TAMANO_ASCENDENTE = 6,
        TAMANO_DESCENDENTE = 7
    }
}
