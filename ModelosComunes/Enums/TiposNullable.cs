﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelosComunes.Enums
{
    /// <summary>
    /// [NULLABLE(0): Permite valores nulos]
    /// [NO_NULLABLE(1): No permite valores nulos, pero si vacíos o espacios]
    /// [NO_NULLABLE_NI_VACIO(2): No permite valores nulos ni vacíos, pero sí espacios]
    /// [NO_NULLABLE_NI_VACIO_NI_ESPACIO(3): No permite valores nulos, vacíos o espacios]
    /// </summary>
    public enum TiposNullable
    {
        NULLABLE = 0,
        NO_NULLABLE = 1,
        NO_NULLABLE_NI_VACIO = 2,
        NO_NULLABLE_NI_VACIO_NI_ESPACIO = 3
    }
}
