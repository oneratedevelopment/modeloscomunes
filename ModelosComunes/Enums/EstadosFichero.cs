﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelosComunes.Enums
{
    /// <summary>
    /// Determina si el fichero se ha encontrado o no o si se ha descartado por no cumplir con normas de validación
    /// </summary>
    public enum EstadosFichero
    {
        DESCONOCIDO = 0,
        ENCONTRADO = 1,
        NO_ENCONTRADO = 2,
        DESCARTADO = 3, 
        NO_APLICA = 4 
    }
}
