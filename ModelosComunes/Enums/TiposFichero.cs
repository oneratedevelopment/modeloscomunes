﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelosComunes.Enums
{
    public enum TiposFichero
    {
        DESCONOCIDO = 0,
        CSV = 1,
        MDB = 2,
        XLS = 3,
        ZIP = 4,
        RAR = 5,
        OTRO = 6
    }
}
