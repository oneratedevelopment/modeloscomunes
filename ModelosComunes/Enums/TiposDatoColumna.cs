﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelosComunes.Enums
{
    public enum TiposDatoColumna
    {
        EXPRESION = 0,
        BOOLEAN = 1,
        STRING = 2,
        INT = 3,
        BIGINT = 4,
        DECIMAL = 5,
        DATE = 6
    }
}
