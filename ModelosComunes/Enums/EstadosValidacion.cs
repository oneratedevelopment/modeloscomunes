﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelosComunes.Enums
{
    /// <summary>
    /// [NO_APLICA(0): NO SE DEBE REALIZAR LA VALIDACIÓN PARA ESTE ELEMENTO O PROPIEDAD] 
    /// [DESCONOCIDO(1): VALIDACIÓN PENDIENTE DE REALIZARSE] 
    /// [OK(2): VALIDACIÓN CORRECTA] 
    /// [KO(3): VALIDACIÓN INCORRECTA] 
    /// [OMITIDO(4): LA VALIDACIÓN SE HA OMITIDO] 
    /// </summary>
    public enum EstadosValidacion
    {
        NO_APLICA = 0,
        DESCONOCIDO = 1,
        OK = 2,
        KO = 3, 
        OMITIDO = 4
    }
}
